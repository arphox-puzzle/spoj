﻿using NUnit.Framework;
using TOANDFRO;

namespace Tests
{
    [TestFixture]
    public sealed class Tests
    {
        [TestCase(5, "toioynnkpheleaigshareconhtomesnlewx", "theresnoplacelikehomeonasnowynightx")]
        [TestCase(3, "ttyohhieneesiaabss", "thisistheeasyoneab")]
        public void SampleTest(int cols, string input, string expectedOutput)
        {
            var solver = new Solver();
            string result = solver.Solve(input, cols);
            Assert.That(result, Is.EqualTo(expectedOutput));
        }
    }
}