﻿using System;
using System.Text;

namespace TOANDFRO
{
    class Program
    {
        static void Main()
        {
            while (true)
            {
                int columns = int.Parse(Console.ReadLine());
                if (columns == 0)
                    return;

                string input = Console.ReadLine();

                var solver = new Solver();
                string result = solver.Solve(input, columns);
                Console.WriteLine(result);
            }
        }
    }

    public sealed class Solver
    {
        // It would be better to solve without using arrays
        // and calculate those tricky string positions,
        // but since still got accepted, I will leave it as it is

        public string Solve(string input, int cols)
        {
            string[] chunks = Chunk(input, cols);
            ReverseEverySecondString(chunks);

            StringBuilder output = new StringBuilder(input.Length);

            int colIndexer = 0;
            for (int i = 0; i < cols; i++)
            {
                for (int c = 0; c < chunks.Length; c++)
                    output.Append(chunks[c][colIndexer]);

                colIndexer++;
            }

            return output.ToString();
        }

        private static string[] Chunk(string input, int chunkSize)
        {
            int numberOfChunks = input.Length / chunkSize;
            string[] output = new string[numberOfChunks];

            for (int i = 0; i < numberOfChunks; i++)
                output[i] = input.Substring(chunkSize * i, chunkSize);

            return output;
        }
        private static void ReverseEverySecondString(string[] input)
        {
            for (int i = 1; i < input.Length; i += 2)
                input[i] = Reverse(input[i]);
        }
        private static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}