// Exported from "my submission history"
using System;
using System.Collections.Generic;
class Program
{
    private static void Main()
    {
        ADDREV.PrintOutput(ADDREV.GetInput());
    }
}
class ADDREV
{
    public static string[] GetInput()
    {
        int lines = int.Parse(Console.ReadLine());
        string[] rows = new string[lines];
        for (int i = 0; i < lines; i++)
        {
            rows[i] = Console.ReadLine();
        }
        return rows;
    }
    public static void PrintOutput(string[] input)
    {
        for (int i = 0; i < input.Length; i++)
        {
            Console.WriteLine(ProcessOneLine(input[i]));
        }
    }
    private static string ProcessOneLine(string inputLine)
    {
        string[] lineArray = inputLine.Split(' ');
        string first = ReverseString(lineArray[0]);
        string second = ReverseString(lineArray[1]);
        int res = int.Parse(first) + int.Parse(second);
        return ReverseString(res.ToString()).TrimStart('0');
    }
    public static string ReverseString(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }
}