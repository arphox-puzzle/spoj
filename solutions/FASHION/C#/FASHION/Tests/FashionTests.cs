﻿using FASHION;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    [TestFixture]
    public sealed class FashionTests
    {
        private static IEnumerable<TestCaseData> TestCases()
        {
            yield return new TestCaseData(new int[] { 1, 1 }, new int[] { 3, 2 }, 5);
            yield return new TestCaseData(new int[] { 2, 3, 2 }, new int[] { 1, 3, 2 }, 15);
        }

        [TestCaseSource(nameof(TestCases))]
        public void SampleTest(int[] a, int[] b, int expectedResult)
        {
            var solver = new Solver();
            int result = solver.Solve(a, b);
            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}