﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ONP
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            var solver = new Solver();
            for (int i = 0; i < n; i++)
            {
                string input = Console.ReadLine();
                string result = solver.Solve(input);
                Console.WriteLine(result);
            }
        }
    }

    public sealed class Solver
    {
        // Note: it seems like we don't really have to cope with surplus brackets or no brackets at all.

        public string Solve(string input)
        {
            StringBuilder output = new StringBuilder(input.Length);
            Stack<char> operatorStack = new Stack<char>();

            foreach (char ch in input)
            {
                if (IsOperand(ch))
                    output.Append(ch);
                else if (ch == '(')
                    operatorStack.Push(ch);
                else if (ch == ')')
                {
                    while(operatorStack.Count > 0 && operatorStack.Peek() != '(')
                        output.Append(operatorStack.Pop());

                    operatorStack.Pop(); // pop '('
                }
                else // by contract, only can be two-arg operator: + - * / ^
                    operatorStack.Push(ch);
            }

            while(operatorStack.Count > 0)
                output.Append(operatorStack.Pop());

            return output.ToString();
        }

        private static bool IsOperand(char character) => character >= 'a' && character <= 'z';
    }
}
