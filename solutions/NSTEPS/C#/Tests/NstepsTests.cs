﻿using NSTEPS;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public sealed class NstepsTests
    {
        [TestCase("4 2", "6")]
        [TestCase("6 6", "12")]
        [TestCase("3 4", "No Number")]
        public void SampleTest(string input, string expectedOutput)
        {
            var solver = new Solver();
            var output = solver.Solve(input);
            Assert.That(output, Is.EqualTo(expectedOutput));
        }

        [TestCase("0 0", "0")]
        [TestCase("1 1", "1")]
        [TestCase("2 0", "2")]
        [TestCase("2 2", "4")]
        [TestCase("5 3", "7")]
        [TestCase("6 4", "10")]
        [TestCase("6 6", "12")]
        [TestCase("7 5", "11")]
        [TestCase("7 4", "No Number")]
        public void Test(string input, string expectedOutput)
        {
            var solver = new Solver();
            var output = solver.Solve(input);
            Assert.That(output, Is.EqualTo(expectedOutput));
        }
    }
}