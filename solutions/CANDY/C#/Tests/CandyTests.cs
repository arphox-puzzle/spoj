﻿using CANDY;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests
{
    [TestFixture]
    public class CandyTests
    {
        private static IEnumerable<TestCaseData> SampleTestCases()
        {
            yield return new TestCaseData(new int[] { 1, 1, 1, 1, 6 }, 4);
            yield return new TestCaseData(new int[] { 1, 1, 1, 1, 7 }, -1);
            yield return new TestCaseData(new int[] { 3, 4 }, -1);
            yield return new TestCaseData(new int[] { 3, 5 }, 1);
            yield return new TestCaseData(new int[] { 3, 6 }, -1);
            yield return new TestCaseData(new int[] { 3, 7 }, 2);
            yield return new TestCaseData(new int[] { 3, 3 }, 0);
            yield return new TestCaseData(new int[] { 3, 3, 4 }, -1);
            yield return new TestCaseData(new int[] { 3, 3, 5 }, -1);
            yield return new TestCaseData(new int[] { 3, 3, 6 }, 2);
            yield return new TestCaseData(new int[] { 1, 2, 3, 2, 6, 4 }, 4);
        }


        [TestCaseSource(nameof(SampleTestCases))]
        public void Test(int[] input, int expectedOutput)
        {
            var solver = new Solver();
            int output = solver.Solve(input);
            Assert.That(output, Is.EqualTo(expectedOutput));
        }
    }
}