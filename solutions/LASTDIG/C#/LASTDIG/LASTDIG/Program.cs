﻿using System;
using System.Linq;

namespace LASTDIG
{
    class Program
    {
        static void Main()
        {
            int t = int.Parse(Console.ReadLine());
            var solver = new Solver();
            for (int i = 0; i < t; i++)
            {
                var input = Console.ReadLine().Split().Select(int.Parse).ToArray();
                int result = solver.Solve(input[0], input[1]);
                Console.WriteLine(result);
            }
        }
    }

    public class Solver
    {
        static int[,] Lookup = new int[,]{
            {0,0,0,0},
            {1,1,1,1},
            {4,8,6,2},
            {9,7,1,3},
            {6,4,6,4},
            {5,5,5,5},
            {6,6,6,6},
            {9,3,1,7},
            {4,2,6,8},
            {1,9,1,9}
        };

        public int Solve(int a, int b)
        {
            if (a == 0 || (a == 0 && b == 0)) return 0;
            if (a == 1 || b == 0) return 1;
            return Lookup[a % 10, (b + 2) % 4];
        }
    }
}