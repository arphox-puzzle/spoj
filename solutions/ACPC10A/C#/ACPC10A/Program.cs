﻿using System;
using System.Linq;

namespace ACPC10A
{
    class Program
    {
        static void Main()
        {
            var solver = new Solver();

            while (true)
            {
                string input = Console.ReadLine();
                if (input == "0 0 0")
                    return; // exit

                string result = solver.Solve(input);
                Console.WriteLine(result);
            }
        }
    }

    public sealed class Solver
    {
        public string Solve(string input)
        {
            int[] numbers = input.Split(' ').Select(int.Parse).ToArray();
            int a = numbers[0];
            int b = numbers[1];
            int c = numbers[2];

            int diff = c - b;
            if (diff == b - a)
                return $"AP {c + diff}";
            else
                return $"GP {c * (c / b)}";
        }
    }
}
