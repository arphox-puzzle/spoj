﻿using System;
using System.Collections.Generic;

namespace COINS
{
    class Program
    {
        static void Main()
        {
            try
            {
                while (true)
                {
                    long input = long.Parse(Console.ReadLine());
                    var solver = new Solver();
                    long result = solver.Solve(input);
                    Console.WriteLine(result);
                }
            }
            catch
            {
                // We need it because we don't know how they end the input,
                // probably some non-convertable number. But this works.
            }
        }
    }

    public class Solver
    {
        private readonly Dictionary<long, long> valueCache = new Dictionary<long, long>()
        {
            { 0, 0 },
            { 1, 1 }
        };

        public long Solve(long input)
        {
            long value;
            if (valueCache.TryGetValue(input, out value))
                return value;

            long p1 = Solve(input / 2);
            long p2 = Solve(input / 3);
            long p3 = Solve(input / 4);

            long max = Math.Max(input, p1 + p2 + p3);

            valueCache[input] = max;
            return max;
        }
    }
}