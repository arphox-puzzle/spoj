﻿using System;
using System.Numerics;

namespace FCTRL2
{
    class Program
    {
        static void Main()
        {
            byte t = byte.Parse(Console.ReadLine());
            var solver = new Solver();
            for (int i = 0; i < t; i++)
            {
                byte input = byte.Parse(Console.ReadLine());
                Console.WriteLine(solver.Solve(input));
            }
        }
    }

    public class Solver
    {
        public string Solve(byte input)
        {
            BigInteger result = 1;

            for (int i = 1; i <= input; i++)
                result *= i;

            return result.ToString();
        }
    }
}